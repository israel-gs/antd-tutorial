import React, {Component} from 'react'
import {Table, Modal, Button, Form, Input} from 'antd'
import {connect} from 'dva'

const FormItem = Form.Item

function mapStateToProps(state) {
    return {
        cardsList: state.cards.cardsList,
        cardsLoading: state.loading.effects['cards/queryList'],
    }
}

class List extends Component {
    state = {
        visible: false
    }

    columns = [
        {
            title: 'Nombre',
            dataIndex: 'name',
        },
        {
            title: 'Descripcion',
            dataIndex: 'desc',
        },
        {
            title: 'Enlace',
            dataIndex: 'url',
        },
    ]

    showModal = () => {
        this.setState({visible: true});
    }

    handleOk = e => {
        const {dispatch, form: {validateFields}} = this.props;

        validateFields((err, values) => {
            if (!err) {
                dispatch({
                    type: 'cards/addOne',
                    payload: values,
                });
                // 重置 `visible` 属性为 false 以关闭对话框
                this.setState({visible: false});
            }
        });
    }

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        })
    }

    componentDidMount() {
        this.props.dispatch({
            type: 'cards/queryList'
        })
    }

    render() {
        const {cardsList, cardsLoading} = this.props;

        const {visible} = this.state;
        const {form: {getFieldDecorator}} = this.props;

        return (
            <div>
                <Table scroll='true' columns={this.columns} dataSource={cardsList} loading={cardsLoading} rowKey="id"/>
                <Button onClick={this.showModal}>Nuevo</Button>
                <Modal visible={this.state.visible} onOk={this.handleOk} onCancel={this.handleCancel}
                       title='Nuevo Registro'>
                    <Form>
                        <FormItem label="Nombre">
                            {getFieldDecorator('name', {
                                rules: [{required: true}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Descripcion">
                            {getFieldDecorator('desc')(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Enlace ">
                            {getFieldDecorator('url', {
                                rules: [{type: 'url'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            </div>
        )
    }
}

export default connect(mapStateToProps)(Form.create()(List));
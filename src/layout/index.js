import {Component} from 'react';
import {Layout, Menu, Icon} from 'antd';

const {Header, Footer, Sider, Content} = Layout;

import Link from 'umi/link';

const SubMenu = Menu.SubMenu;

class BasicLayout extends Component {
    render() {
        return (
            <Layout>
                <Sider
                    breakpoint="lg"
                    collapsedWidth="0"
                    onBreakpoint={broken => {
                        console.log(broken);
                    }}
                    onCollapse={(collapsed, type) => {
                        console.log(collapsed, type);
                    }}
                    width={256} style={{minHeight: '100vh', color: 'white'}}>

                    <div style={{height: '32px', background: 'rgba(255,255,255,.2)', margin: '16px'}}/>
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1">
                            <Link to="/helloworld">
                                <Icon type="pie-chart"/>
                                <span>Helloworld</span>
                            </Link>
                        </Menu.Item>
                        <SubMenu
                            key="sub1"
                            title={<span><Icon type="dashboard"/><span>Dashboard</span></span>}
                        >
                            <Menu.Item key="sub1-2"><Link to="/dashboard/analysis">Analysis</Link></Menu.Item>
                            <Menu.Item key="sub1-3"><Link to="/dashboard/monitor">Monitor</Link></Menu.Item>
                            <Menu.Item key="sub1-4"><Link to="/dashboard/workplace">Workplace</Link></Menu.Item>
                        </SubMenu>
                        <Menu.Item key="2">
                            <Link to="/list">
                                <Icon type="table" />
                                <span>Lista</span>
                            </Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{background: '#fff', textAlign: 'center', padding: 0}}>Header</Header>
                    <Content style={{margin: '24px 16px 0'}}>
                        <div style={{padding: 24, background: '#fff', minHeight: 360}}>
                            {this.props.children}
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>Ant Design ©2018 Created by Ant UED</Footer>
                </Layout>
            </Layout>
        )
    }
}

export default BasicLayout;